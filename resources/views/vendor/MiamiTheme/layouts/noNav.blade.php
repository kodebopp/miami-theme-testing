@include("MiamiTheme::partials.top")
<div class="row">
    <div class="col-xs-12">
        @yield('content')
    </div>
</div>
@include("MiamiTheme::partials.bottom")
