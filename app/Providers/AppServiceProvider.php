<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
//        \Menu::make('AppNavMenu', function ($menu) {
//            $menu->add('Home', ['route' => 'home']);
//            $menu->add('Welcome', ['route' => '/']);
//            $menu->add('Impersonation', ['route' => '/'])->data('permission', 'impersonate-users');
//            $menu->add('Exemptions', ['route' => '/'])->data('permission', 'manage-impersonation-exemptions');
//        });
    }
}
