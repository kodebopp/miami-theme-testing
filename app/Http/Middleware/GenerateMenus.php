<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Konekt\Menu\Facades\Menu;
use Konekt\Menu\Item;
use function dd;
use function var_dump;

class GenerateMenus
{
    /**
     * Handle an incoming request.
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

//        \Menu::make('AppNavMenu', function ($menu) {
//            $menu->add('Home', ['route' => 'test']);
//        $menu->add('Office', ['url' => '/office]);
//        });

        $sidebar = Menu::create('AppNavMenu', ['share' => true]);
        $sidebar->addItem('Home', 'Home', '/');
        $sidebar->addItem('Office', 'office', '/office');
        $sidebar->addItem('About', 'about', ['route' => 'test'])->data('color', 'red');

        $sidebar->items = $sidebar->items->filter(function (Item $item) {
            if (empty($item->data('color'))) {
                return true;
            }
            if (Auth::check()) {
                return Auth::user()->can($item->data('color'));
            }
            return false;
        });
        return $next($request);
    }
}
